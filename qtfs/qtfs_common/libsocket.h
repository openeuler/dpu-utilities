/******************************************************************************
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.
 * qtfs licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 * Author: Liqiang
 * Create: 2023-11-23
 * Description: socket api in user-mode
 *******************************************************************************/

#ifndef __LIB_SOCKET_H__
#define __LIB_SOCKET_H__

enum libsock_cs_e {
	LIBSOCK_CLIENT = 1,
	LIBSOCK_SERVER,
};

int libsock_accept(int sockfd, int family);
int libsock_build_inet_connection(char *ip, unsigned short port, enum libsock_cs_e cs);
int libsock_build_vsock_connection(unsigned int cid, unsigned int port, enum libsock_cs_e cs);

#endif

