# 概述

本文档介绍DPU-OS的背景需求及整体设计思想，并介绍基于openEuler操作系统裁剪构建DPU-OS镜像的方法以及部署验证方法。该特性基于openEuler生态构建轻量化以及极致性能的DPU-OS，为DPU场景及用户提供DPU-OS参考实现。

本文档适用于使用openEuler系统并希望了解和使用DPU的社区开发者、DPU厂商及客户。使用人员需要具备以下经验和技能：

- 熟悉Linux基本操作。

- 熟悉Linux系统构建及部署的相关基础知识和操作。

- 对openEuler ImageTailor镜像裁剪工具有一定了解。
