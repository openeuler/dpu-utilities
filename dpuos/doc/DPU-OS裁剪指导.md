# DPU-OS裁剪指导

本文档主要介绍`imageTailor`的使用方法并结合[dpu-utilities仓库](https://gitee.com/openeuler/dpu-utilities/tree/master/dpuos)的`dpuos`配置文件裁剪得到`dpuos`的安装镜像，具体步骤如下：

#### 准备imageTailor和所需的rpm包

参照[imageTailor使用指导文档](https://docs.openeuler.org/zh/docs/22.03_LTS/docs/TailorCustom/imageTailor%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.html)安装好`imageTailor`工具并将裁剪所要用到的rpm包准备好。

可以使用openEuler提供安装镜像作为镜像裁剪所需要rpm包源，`openEuler-22.03-LTS-everything-debug-aarch64-dvd.iso`中的rpm比较全但是此镜像很大，可以用镜像`openEuler-22.03-LTS-aarch64-dvd.iso`中的rpm包和一个`install-scripts.noarch`实现。

`install-scripts.noarch`包括可以从everything包中获取，或者在系统中通过yum下载：

```bash
yum install -y --downloadonly --downloaddir=./ install-scripts
```

#### 拷贝dpuos相关的配置文件

`imageTailor`工具默认安装在`/opt/imageTailor`路径下。执行下面的命令将`dpuos`的配置拷贝到对应的路径下，拷贝时选择对应架构目录。当前DPU-OS裁剪配置库支持x86_64和aarch64两种架构。

```bash
cp -rf custom/cfg_dpuos /opt/imageTailor/custom
cp -rf kiwi/minios/cfg_dpuos /opt/imageTailor/kiwi/minios/cfg_dpuos
```

#### 修改其他配置文件

* 修改`kiwi/eulerkiwi/product.conf`，增加一行`dpuos`相关配置:

```
dpuos           PANGEA        EMBEDDED   DISK     GRUB2        install_mode=install install_media=CD install_repo=CD selinux=0
```

* 修改`kiwi/eulerkiwi/minios.conf`，增加一行`dpuos`的相关配置:

```
dpuos      kiwi/minios/cfg_dpuos yes
```

* 修改`repos/RepositoryRule.conf`，增加一行`dpuos`的相关配置:

```
dpuos          1           rpm-dir     euler_base
```

#### 设置密码

进入到`/opt/imageTailor`子目录下，修改下面3个文件的密码:

* `custom/cfg_dpuos/usr_file/etc/default/grub`

* `custom/cfg_dpuos/rpm.conf`

* `kiwi/minios/cfg_dpuos/rpm.conf`

密码生成及修改方法可详见openEuler imageTailor手册[配置初始密码](https://docs.openeuler.org/zh/docs/22.03_LTS/docs/TailorCustom/imageTailor%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.html#%E9%85%8D%E7%BD%AE%E5%88%9D%E5%A7%8B%E5%AF%86%E7%A0%81)章节。

#### 执行裁剪命令

执行下面的命令进行裁剪，最后裁剪出来的iso在`/opt/imageTailor/result`路径下：

```bash
cd /opt/imageTailor
./mkdliso -p dpuos -c custom/cfg_dpuos --sec --minios force
```
