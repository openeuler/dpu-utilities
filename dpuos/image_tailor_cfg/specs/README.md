## 介绍

用户可以为DPU-OS自定义软件包及特性，自定义包构建成rpm后放置到DPU-OS特定目录构建ISO。

本目录用来存放用于制作rpm包的spec文件。

## kernel.spec

用于制作自定义内核的kernel spec文件。

#### 制作步骤：
* 下载对应src包：https://repo.openeuler.org/openEuler-22.03-LTS-SP2/source/Packages/ 。 从链接中下载kernel src rpm包
* 使用kernel.spec.patch修改rpm包中的kernel.spec文件，使能kernel制定特性。
* 使用rpmbuild工具构建新的kernel rpm包。并替换至image-Tailor中rpm源。

#### 当前特性
* 开启内核vDPA特性
